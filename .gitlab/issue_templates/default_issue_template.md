## Descrição
<!-- Espaço destinado a adicionar a descrição da issue -->

## Tarefas
<!-- (Opcional) Espaço destinado a especificar as tarefas que serão necessárias para concluir a issue -->
- [ ] Exemplo de tarefa

## Arquivos de apoio
<!-- (Opcional) ço destinado a vídeos, imagens, prints que auxiliem na explicação da issue -->
<!-- ![Nome do arquivo](https://nome_do_arquivo.svg) -->

## Critérios de aceitação
<!-- Espaço destinado a especificar os critérios de aceitação para definir a issue como finalizada -->
- [ ] Exemplo de critério de aceitação
